package io.holub.corva.common.view

import android.widget.ImageView
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target

fun initImageViewViaGlideRes(imageView: ImageView?, photoRes: Int) {
    imageView?.let {
        GlideApp.with(imageView.context)
            .load(photoRes)
            .apply(
                RequestOptions ()
                    .fitCenter()
                    .format(DecodeFormat.PREFER_ARGB_8888)
                    .override(Target.SIZE_ORIGINAL)
            )
            .into(imageView)
    }
}

fun initImageViewViaGlide(imageView: ImageView?, photoUrl: String) {
    imageView?.let {
        GlideApp.with(imageView.context)
            .load(photoUrl)
            .centerCrop()
            .into(imageView)
    }
}
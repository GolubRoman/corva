package io.holub.corva.common.util

import android.app.Activity

import com.tbruyelle.rxpermissions2.Permission
import com.tbruyelle.rxpermissions2.RxPermissions

import io.reactivex.Observable

object PermissionUtils {
    fun requestEachPermissions(activity: Activity, permissions: Array<String>): Observable<Permission> {
        return RxPermissions(activity).requestEach(*permissions)
    }
}

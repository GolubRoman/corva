package io.holub.corva.common.view

import android.support.v4.view.ViewPager

interface OnPageSelectedListener: ViewPager.OnPageChangeListener {
    override fun onPageScrollStateChanged(state: Int) {}
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
}
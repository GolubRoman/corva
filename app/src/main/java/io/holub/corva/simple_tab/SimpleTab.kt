package io.holub.corva.simple_tab

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SimpleTab(
    val backgroundColorRes: Int,
    val imageRes: Int
) : Parcelable
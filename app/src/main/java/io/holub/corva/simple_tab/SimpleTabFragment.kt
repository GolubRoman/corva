package io.holub.corva.simple_tab

import android.annotation.SuppressLint
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.holub.corva.R
import io.holub.corva.common.view.initImageViewViaGlideRes
import io.holub.corva.databinding.FragmentSimpleTabBinding

class SimpleTabFragment: Fragment() {

    private var binding: FragmentSimpleTabBinding? = null

    @SuppressLint("CheckResult")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_simple_tab, container, false)

        arguments?.let {
            if (it.getParcelable<SimpleTab>(SIMPLE_TAB_KEY) != null) {
                binding?.simpleTab = it.getParcelable(SIMPLE_TAB_KEY)
                binding?.executePendingBindings()
                initImageViewViaGlideRes(binding?.ivImage, it.getParcelable<SimpleTab>(SIMPLE_TAB_KEY).imageRes)
            }
        }
        return binding!!.root
    }

    companion object {
        const val SIMPLE_TAB_KEY = "SIMPLE_TAB_KEY"

        fun newInstance(simpleTab: SimpleTab): SimpleTabFragment {
            val fragment = SimpleTabFragment()

            val args = Bundle()
            args.putParcelable(SIMPLE_TAB_KEY, simpleTab)
            fragment.arguments = args

            return fragment
        }
    }
}

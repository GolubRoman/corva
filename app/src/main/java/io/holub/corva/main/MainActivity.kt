package io.holub.corva.main

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import io.holub.corva.gallery.GalleryFragment
import io.holub.corva.common.view.OnPageSelectedListener
import io.holub.corva.R
import io.holub.corva.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(binding?.appBarMain?.toolbar)

        initViewPager()
        initNavigation()
    }

    private fun initNavigation() {
        val toggle = ActionBarDrawerToggle(
            this, binding?.drawerLayout,
            binding?.appBarMain?.toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        binding?.drawerLayout?.addDrawerListener(toggle)
        binding?.navView?.setNavigationItemSelectedListener(this)
        toggle.syncState()
    }

    override fun onBackPressed() {
        if (binding?.drawerLayout?.isDrawerOpen(GravityCompat.START) == true) {
            binding?.drawerLayout?.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        binding?.appBarMain?.contentMain?.vpPages?.setCurrentItem(mapButtonIdToPagePosition(item.itemId), true)
        binding?.drawerLayout?.closeDrawer(GravityCompat.START)
        return true
    }

    private fun mapButtonIdToPagePosition(itemId: Int) = when (itemId) {
        R.id.nav_gallery -> 0
        R.id.nav_tab1 -> 1
        R.id.nav_tab2 -> 2
        R.id.nav_tab3 -> 3
        else -> 0
    }

    private fun initViewPager() {
        val adapter = TabPagerAdapter(supportFragmentManager, this)
        val viewPager = binding?.appBarMain?.contentMain?.vpPages
        val tlTabs = binding?.appBarMain?.contentMain?.tlTabs
        viewPager?.adapter = adapter
        viewPager?.offscreenPageLimit = adapter.count
        tlTabs?.setupWithViewPager(viewPager)
        Handler().postDelayed({ (adapter.getItem(0) as GalleryFragment).init() }, 300)
        viewPager?.addOnPageChangeListener(object : OnPageSelectedListener {
            override fun onPageSelected(position: Int) {
                if (adapter.getItem(position) is GalleryFragment) {
                    Handler().postDelayed({ (adapter.getItem(position) as GalleryFragment).init() }, 300)
                }
            }
        })
    }
}

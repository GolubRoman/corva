package io.holub.corva.main

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import io.holub.corva.gallery.GalleryFragment
import io.holub.corva.R
import io.holub.corva.simple_tab.SimpleTab
import io.holub.corva.simple_tab.SimpleTabFragment
import java.util.Arrays.asList

internal class TabPagerAdapter(fm: FragmentManager, context: Context) : FragmentPagerAdapter(fm) {

    private val fragments = asList(
        GalleryFragment(),
        SimpleTabFragment.newInstance(
            SimpleTab(
                context.resources.getColor(R.color.tab_1_background_color),
                R.drawable.ic_number_1
            )
        ),
        SimpleTabFragment.newInstance(
            SimpleTab(
                context.resources.getColor(R.color.tab_2_background_color),
                R.drawable.ic_number_2
            )
        ),
        SimpleTabFragment.newInstance(
            SimpleTab(
                context.resources.getColor(R.color.tab_3_background_color),
                R.drawable.ic_number_3
            )
        )
    )

    private val titles: List<String> = asList(
        context.resources.getString(R.string.menu_gallery),
        context.resources.getString(R.string.menu_tab1),
        context.resources.getString(R.string.menu_tab2),
        context.resources.getString(R.string.menu_tab3)
    )

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

}
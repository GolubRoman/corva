package io.holub.corva.gallery

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.holub.corva.R
import io.holub.corva.common.util.PermissionUtils
import io.holub.corva.databinding.FragmentGalleryBinding
import io.reactivex.disposables.Disposable

class GalleryFragment: Fragment() {

    private var binding: FragmentGalleryBinding? = null
    private var viewModel: GalleryViewModel? = null
    private var adapter = GalleryAdapter()
    private var isInitted = false
    private var permissionDisposable: Disposable? = null
    private val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)

    @SuppressLint("CheckResult")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_gallery, container, false)
        viewModel = ViewModelProviders.of(this).get(GalleryViewModel::class.java)
        return binding?.root
    }

    fun init() {
        requestEssentialPermission {
            if (!isInitted && viewModel != null && binding != null) {
                showProgressBarAndDisableButtons()
                binding?.rvPhotos?.layoutManager = LinearLayoutManager(activity)
                binding?.rvPhotos?.adapter = adapter
                viewModel?.getAllImagesData(activity?.contentResolver)?.observe(this, Observer {
                    it?.let { nonNullList ->
                        if (nonNullList.isNotEmpty()) {
                            adapter.submitList(nonNullList)
                        }
                        hideProgressBarAndEnableButtons()
                    }
                })
                isInitted = true
            }
        }
    }

    private inline fun requestEssentialPermission(crossinline body: () -> Unit) {
        activity?.let {
            permissionDisposable = PermissionUtils.requestEachPermissions(activity as AppCompatActivity, PERMISSIONS)
                .take(PERMISSIONS.size.toLong()).toList()
                .subscribe({ data ->
                    if (data.none { !it.granted }) {
                        body()
                        hideUnapprovedMapView()
                    } else {
                        showUnapprovedMapView()
                    }
                }, { Log.e("Error", it.message) })
        }
    }

    override fun onPause() {
        super.onPause()
        if (permissionDisposable?.isDisposed == true) { permissionDisposable?.dispose() }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showProgressBarAndDisableButtons() {
        binding?.flProgressBar?.visibility = View.VISIBLE
        binding?.flProgressBar?.setOnTouchListener { _, _ -> true }
    }

    private fun hideProgressBarAndEnableButtons() {
        binding?.flProgressBar?.visibility = View.INVISIBLE
    }

    private fun showUnapprovedMapView() {
        hideProgressBarAndEnableButtons()
        binding?.let {
            it.llUnapprovedPermissionContainer.visibility = View.VISIBLE
        }
    }

    private fun hideUnapprovedMapView() {
        binding?.let {
            it.llUnapprovedPermissionContainer.visibility = View.INVISIBLE
        }
    }
}

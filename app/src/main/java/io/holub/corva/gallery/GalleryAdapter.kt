package io.holub.corva.gallery

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.holub.corva.R
import io.holub.corva.common.view.initImageViewViaGlide
import io.holub.corva.databinding.ItemPhotoBinding

class GalleryAdapter : RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder>() {

    private var list = arrayListOf<String?>()

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        val binding = DataBindingUtil.inflate<ItemPhotoBinding>(LayoutInflater.from(parent.context),
            R.layout.item_photo, parent, false)
        return GalleryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        holder.bindTo(list[position])
    }

    fun submitList(list: List<String?>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    inner class GalleryViewHolder(val binding: ItemPhotoBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(photoUrl: String?) {
            photoUrl?.let { initImageViewViaGlide(binding?.ivImage, photoUrl) }
        }
    }
}

package io.holub.corva.gallery

import android.arch.lifecycle.LiveDataReactiveStreams
import android.arch.lifecycle.ViewModel
import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.provider.MediaStore.MediaColumns
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GalleryViewModel : ViewModel() {

    fun getAllImagesData(contentResolver: ContentResolver?) = LiveDataReactiveStreams.fromPublisher(Flowable.fromCallable {
        val uri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val cursor: Cursor?
        val columnIndexData: Int
        val listOfAllImages = mutableListOf<String>()
        var absolutePathOfImage: String?

        val projection = arrayOf(MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME)

        cursor = contentResolver?.query(uri, projection, null, null, null)
        cursor?.let { cursor ->
            columnIndexData = cursor.getColumnIndexOrThrow(MediaColumns.DATA)
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(columnIndexData)
                absolutePathOfImage?.let { listOfAllImages.add(it) }
            }

            if (!cursor.isClosed) { cursor.close() }
        }
        listOfAllImages
    }.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread()))

}


